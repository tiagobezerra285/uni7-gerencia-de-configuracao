
resource "aws_instance" "app" {
  ami = var.ami["us-east-1"]
  instance_type = var.tipo_instancia
  key_name = "Uni7-trabalhoFinal"
  subnet_id = aws_subnet.Public_subnet.id
  vpc_security_group_ids = [aws_security_group.allow_ssh.id,
                            aws_security_group.web_egress.id,
                            aws_security_group.allow_http.id,
                            
                            ]
   tags = {
      Name = "Servidor de Aplicação"
  }
  depends_on = [aws_s3_bucket.ansible_bucket]
  //user_data = templatefile("scripts/prepara_web.sh.tpl", { db_instance_ip = aws_instance.db.private_ip })
}



resource "aws_instance" "db" {
    ami = var.ami["us-east-1"]
    instance_type = var.tipo_instancia
    key_name = "Uni7-trabalhoFinal"
    subnet_id = aws_subnet.Private_subnet.id
    vpc_security_group_ids = [aws_security_group.allow_ssh.id, aws_security_group.web_egress.id, aws_security_group.allow_mysql.id]
    tags = {
        Name = "db"
    }
    depends_on = [aws_s3_bucket.ansible_bucket]
    //user_data = file("scripts/prepara_bd.sh")
}

