
resource "aws_s3_bucket" "ansible_bucket" {
  bucket = "ansible-config-tiagobezerra"
  acl    = "public-read"
  tags = {
        Name = "bucket_ansible"
    }
}


resource "aws_s3_bucket_object" "upload" {
  bucket = aws_s3_bucket.ansible_bucket.bucket
  key    = "ansible/ansible.zip"
  source = "ansible/ansible.zip"
  acl = "public-read"
  tags = {
      Name = "ansible-trabalho-final"
  }
}