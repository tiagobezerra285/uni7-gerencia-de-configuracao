variable "tipo_instancia" {
  type        = string
  default     = "t2.micro"
  description = "description"
}


variable "ami" {
    type = map
    default = {
        "us-east-1" = "ami-0ac80df6eff0e70b5"
    }
}

variable "vpcCIDRblock" {
    default = "10.0.0.0/16"
}

variable "instanceTenancy" {
    default = "default"
}

variable "dnsSupport" {
    default = true
}
variable "dnsHostNames" {
    default = true
}

variable "availabilityZonea" {
     default = "us-east-1a"
}

variable "mapPublicIP" {
    default = true
}

variable "publicdestCIDRblock" {
    default = "0.0.0.0/0"
}

variable "publicsCIDRblock" {
    default = "10.0.1.0/24"
}


variable "privatesCIDRblock" {
    default = "10.0.2.0/24"
}